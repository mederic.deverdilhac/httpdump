package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"time"

	"github.com/gin-gonic/gin"
)

func main() {
	gin.DisableConsoleColor()
	// f, _ := os.Create("httpdump.log")
	// gin.DefaultWriter = io.MultiWriter(f, os.Stdout)

	router := gin.New()

	// LoggerWithFormatter middleware will write the logs to gin.DefaultWriter
	// By default gin.DefaultWriter = os.Stdout
	router.Use(gin.LoggerWithFormatter(func(param gin.LogFormatterParams) string {

		// custom format
		return fmt.Sprintf("%s - [%s] %s | %s | %s | %d | %s | %s\n",
			param.ClientIP,
			param.TimeStamp.Format(time.RFC1123),
			param.Method,
			param.Path,
			getHeadersAsString(param.Request.Header),
			param.StatusCode,
			param.Request.Proto,
			param.ErrorMessage,
		)
	}))

	router.Use(gin.Recovery())

	router.GET("/*path", mirror)
	router.POST("/*path", mirror)
	router.PUT("/*path", mirror)
	router.DELETE("/*path", mirror)
	router.PATCH("/*path", mirror)
	router.HEAD("/*path", mirror)
	router.OPTIONS("/*path", mirror)

	// By default it serves on :8080 unless a
	// PORT environment variable was defined.
	router.Run()
}

func mirror(c *gin.Context) {
	if os.Getenv("HTTPDUMP_FILE") != "" {
		dumpToFile(c)
	}
	c.String(http.StatusOK, "OK")
}

func dumpToFile(c *gin.Context) {
	filename := os.Getenv("HTTPDUMP_FILE")

	// If the file doesn't exist, create it, or append to the file
	f, err := os.OpenFile(filename, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	requestBody, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		log.Fatal(err)
	}

	requestDetails := fmt.Sprintf("%s | %s | %s | %s\n",
		c.Request.Method,
		c.Request.URL.RequestURI(),
		getOneLineBody(requestBody),
		getHeadersAsString(c.Request.Header),
	)

	if _, err := f.Write([]byte(requestDetails)); err != nil {
		log.Fatal(err)
	}
}

func getHeadersAsString(structHeaders http.Header) string {
	headers := ""
	for k, v := range structHeaders {
		headers = fmt.Sprintf("%s %s=%s", headers, k, v)
	}
	return headers
}

func getOneLineBody(body []byte) string {
	return url.QueryEscape(string(body))
}
