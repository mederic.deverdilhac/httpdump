FROM golang:1.19-alpine as builder

WORKDIR /app
COPY go.mod ./
COPY go.sum ./
RUN go mod download

COPY *.go ./

RUN go build -o /httpdump

FROM alpine:3.17

COPY --from=builder /httpdump /httpdump
ENV HTTPDUMP_FILE=""
CMD ["/httpdump"]
