# httpdump

Simple http server logging incoming requests. Usefull for testing some http output

Used with env variable HTTPDUMP_FILE=<log_file> provided a logfile containing request details.

Container image : `registry.gitlab.com/mederic.deverdilhac/httpdump:main`
